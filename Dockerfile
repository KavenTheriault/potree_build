FROM node:10.7.0

RUN git clone https://github.com/potree/potree.git
WORKDIR /potree

RUN npm install --save \
    && npm install -g gulp \
    && npm install -g rollup

RUN gulp build
RUN rollup -c